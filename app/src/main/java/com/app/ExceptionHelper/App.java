package com.app.ExceptionHelper;

import com.app.ExceptionHelper.UncaughtExceptionHandlerHelper.UncaughtExceptionHandlerImpl;

import io.dcloud.PandoraEntry;
import io.dcloud.application.DCloudApplication;

public class App extends DCloudApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        UncaughtExceptionHandlerImpl.getInstance().init(this, false, true, 0, PandoraEntry.class);
    }
}
