package com.app.ExceptionHelper.UncaughtExceptionHandlerHelper;

import android.util.Log;

import com.blankj.utilcode.util.LogUtils;

import java.util.Arrays;

import cn.hpb.serialportlibrary.BuildConfig;


/**
 * Created by hpb
 * On 2019/8/28 17:11
 */
public class LogCat {
    private static final String TAG = "LOG";
    public static void d(final Object... contents){
        if(BuildConfig.DEBUG){
            Log.d(TAG, Arrays.toString(contents));
        }else{
            LogUtils.file(LogUtils.D, contents);
        }
    }
    public static void e(final Object... contents){
        if(BuildConfig.DEBUG){
            Log.e(TAG, Arrays.toString(contents));
        }else{
            LogUtils.file(LogUtils.E, contents);
        }
    }
}


